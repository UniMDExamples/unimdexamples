// unimdExample.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//
#include "stdafx.h"
#include <iostream>
#include <string>
#include <sstream>
#include <malloc.h>

using namespace std;

/*
3a)	Definieren Sie eine Klasse Auto mit den privaten Attributen
hersteller (Datentyp string, z.B. �VW�, �Opel�),
alter   (Alter des Autos in Jahren, Datentyp int),
neuPreis  (Datentyp double)
*/
class Auto{
private:
	string m_hersteller;
	int m_alter;
	double m_neuPreis;
public:
	Auto();
	Auto(string, int, double);
	void read();
	string toString();
	double gebrauchtPreis();
};

void read(string &text, int &anz);
void output(string text, int anz);
bool check(int feld[], int wert);
void initArray(int feld[]);
int treffer(int feld[], int tippschein[]);
Auto teuerstesAuto(Auto a[], int n);
double kapital(Auto a[], int n);

int _tmain(int argc, _TCHAR* argv[])
{
	/*
	1.	Bei diesem Programm soll zun�chst ein Text (als string) eingelesen werden. Au�erdem soll eine Anzahl eingelesen werden,
		die angibt, wie oft der Text anschlie�end ausgegeben werden soll. Anschlie�end soll der Text entsprechend der Anzahl ausgegeben werden.

	c)	In der main - Funktion sollen die beiden Variablen deklariert, mit der Funktion read aus
	a)	initialisiert und mit der Funktion output aus
	b)	auf dem Bildschirm ausgegeben werden.
	*/

	int anz = 0;
	string text = "";

	read(text, anz);
	output(text, anz);

	// **************************************

	/*
	2.	In einem Feld mit 6 Feldelementen sollen Lottozahlen abgelegt werden. Anschlie�end soll mit einer weiteren Funktion gepr�ft werden
		wie viele der Lottozahlen mit einem gegebenen Tippschein �bereinstimmen. Hinweis. Als Feldgr��e wird f�r alle 4 Aufgabenteile jeweils 6 angenommen.

	d)	Schreiben sie die main()-Funktion, in der zwei statische Felder mit jeweils 6 Feldelementen deklariert werden. Lesen Sie f�r den Tippschein,
		6 Werte in ein Feld ein. Rufen sie f�r das anderes Feld die Funktion initArray auf, um es mit zuf�lligen Lottozahlen zu f�llen.
		Pr�fen Sie anschlie�end mit der Funktion treffer die beiden Felder gegeneinander und geben Sie die Anzahl der gleichen Werte aus.
	*/

	int feld1[6];
	int feld2[6];
	int counterHits = 0;

	for (size_t i = 0; i < 6; i++)
	{
		int newValue;
		bool containsValue = false;

		cout << "Bitte geben Sie die " << i + 1 << ". Zahl ein: ";
		cin >> newValue;

		if (check(feld1, newValue))
		{
			cout << "Der Wert " << newValue << " wird bereits verwendet. Bitte Wiederholen Sie die Eingabe.";
			i--;
		}
		else
		{
			feld1[i] = newValue;
		}
	}

	initArray(feld2);
	counterHits = treffer(feld2, feld1);
	cout << counterHits << " Treffer wurden erzielt." << endl;

	// **************************************

	/*
	3.	Ihre Aufgabe ist es, eine Klasse Auto zu schreiben . F�r jedes Auto soll es Attribute f�r den Hersteller,  das Alter (in Jahren) und den Neupreis geben.
		Au�erdem soll es eine Methode geben, die auf vereinfachte Weise aus Alter und Neupreis den Gebrauchtpreis des Autos berechnet.
		Im Hauptprogramm soll dann eine kleine Autowerkstatt simuliert werden. In dieser Werkstatt soll das teuerste Gebrauchtauto und der aktuelle Wert aller Fahrzeuge ermittelt und auf dem Bildschirm ausgegeben werden.
		Gehen Sie bei der Bearbeitung dieser Aufgabe in folgenden Schritten vor:
	*/

	Auto *auto1 = new Auto();
	auto1->read();

	Auto *auto2 = new Auto("Opel", 25, 1000.00);
	cout << auto1->toString() << endl;
	cout << auto2->toString() << endl;

	int anzahlAutos;

	cout << "Wieviel Fahrzeuge kann die Werkstatt fassen: ";
	cin >> anzahlAutos;

	Auto *autos = (Auto *)calloc(anzahlAutos, sizeof(Auto));

	if (autos != NULL)
	{
		for (int i = 0; i < anzahlAutos; i++)
		{
			Auto *newAuto = new Auto();

			autos[i] = *newAuto;
			autos[i].read();
		}

		cout << "Teuerstes Auto: " << endl << teuerstesAuto(autos, anzahlAutos).toString() << endl;
		cout << "Gesamtkapital: " << endl << kapital(autos, anzahlAutos) << endl;

		free(autos);
	}

	// **************************************

	cout << "Dr�cke eine beliebige Taste um das Fenster zu schlie�en.";																	// Wird ausgef�hrt, damit die Konsole nicht geschlossen wird.
	getchar();

	return 0;
}

/*
1a)	Schreiben Sie die Funktion read, die einen Text einliest und diesen in einer Variablen vom Typ string ablegt.
	Au�erdem soll ein Wert f�r die Anzahl der Wiederholungen  vom Typ int eingelesen werden. Dabei soll die Eingabe so lange wiederholt werden,
	bis ein Wert gr��er �0� eingegeben wurde.
*/
void read(string &text, int &anz)
{
	cout << "Geben Sie bitte einen beliebigen Text ein: ";
	cin >> text;			// Text wird von Tastatus eingelesen. Leerzeichen und Entertaste beenden die Eingabe. 

	cout << "Geben sie bitte die Anzahl der Wiederholungen an: ";
	while (anz <= 0)		// Die Eingabe wird solange wiederholt, bis der Wert gr��er 0 ist. 
	{
		cin >> anz;			// Eingabe der Wiederholungen. 
	}
}

/*
1b)	Die Funktion output bekommt zwei Werte als Parameter (1x string, 1x int) �bergeben. In der Funktion soll nun der Wert des Parameters vom Typ string so oft ausgeben werden,
	wie der in dem Parameter vom Typ int hinterlegte Wert gro� ist. Die Ausgabe soll jeweils auf einer neuen Zeile erfolgen.
*/
void output(string text, int anz)
{
	for (size_t i = 0; i < anz; i++)
	{
		cout << text << endl;							// Text wird ausgegeben
	}
}

/*
2a)	Schreiben Sie eine Funktion check, mit der gepr�ft wird, ob sich ein gegebener Wert bereits in einem Feld befindet.
	Ist dies der Fall, soll die Funktion den Wahrheitswert true zur�ckgeben, ansonsten den Wert false
*/
bool check(int feld[], int wert)
{
	for (size_t i = 0; i < sizeof(feld); i++)
	{
		if (feld[i] == wert)
			return true;
	}

	return false;
}

/*
2b)	Schreiben Sie eine Funktion initArray, die ein Feld mit 6 Zufallszahlen zwischen 1 und 49 f�llt (rand() liefert eine ganze Zahl zwischen 0 und RAND_MAX).
	Pr�fen Sie dabei, dass keine doppelten Werte im Feld abgelegt werden. Sie k�nnen daf�r die Funktion check nutzen. (
*/
void initArray(int feld[])
{
	for (size_t i = 0; i < 6; i++)
	{
		int newValue = 0;

		do
		{
			newValue = rand() % 50;
		} while (check(feld, newValue));

		feld[i] = newValue;
	}
}

/*
2c	Schreiben Sie eine Funktion treffer, die ein Feld (Lottozahlen) mit 6 Zufallszahlen zwischen 1 und 49 gegen einen gegebenen Tippschein
	(Feld mit 6 Feldelementen) pr�ft. Es soll dabei gez�hlt werden, wie viele gleiche Werte in den Lottozahlen und dem Tippschein existieren.
	Geben Sie das Ergebnis an die aufrufende Funktion zur�ck.
*/
int treffer(int feld[], int tippschein[])
{
	int counter = 0;

	for (size_t i = 0; i < sizeof(tippschein); i++)
	{
		if (check(feld, tippschein[i]))
			counter++;
	}

	return counter;
}

/*
� einen Standard-Konstruktor (initialisiert die drei Attribute mit beliebigen von Ihnen selbst gew�hlten Werten) und
*/
Auto::Auto()
{
	m_hersteller = "Audi";
	m_alter = 2;
	m_neuPreis = 27000.00;
} 

/*
� einen Konstruktor mit 3 Parametern (f�r die drei Attribute hersteller, alter, neuPreis)
*/
Auto::Auto(string herst, int alter, double pr)
{     
	m_hersteller = herst;
	m_alter = alter;
	m_neuPreis = pr;
}

/*
� die Methode read zum Einlesen der Attribute von der Tastatur  (ohne Kontrolle)
*/
void  Auto::read()
{        
	cout << "Hersteller: ";
	cin >> m_hersteller;
	cout << "Alter: ";
	cin >> m_alter;
	cout << "Neupreis:";
	cin >> m_neuPreis;
}

/*
� die Methode toString() zum Darstellen aller drei Attribute als string und
*/
string Auto::toString()
{   
	std::stringstream s;   

	s << "Hersteller: " << m_hersteller << endl << "Alter: " << m_alter << endl << "Neupreis: " << m_neuPreis;
	return s.str();  // convert stream s into a string 
} 

/*
� die Methode gebrauchtPreis()    
zur Berechnung des Gebrauchtpreises nach der Formel neuPreis/alter.
*/
double Auto::gebrauchtPreis()
{
	return m_neuPreis / m_alter;
}

/*
3b)	Die Funktion teuerstesAuto soll aus den n Elementen des Feldes a das Auto mit dem h�chsten Gebrauchtpreis 
	berechnen und zur�ck liefern (Sie d�rfen davon ausgehen, dass das teuerste Auto eindeutig ist).
*/
Auto teuerstesAuto(Auto a[], int n)
{   
	Auto teuerstesAuto = a[0];

	for (size_t i = 1; i < n; i++)
	{
		if (a[i].gebrauchtPreis() > teuerstesAuto.gebrauchtPreis())
			teuerstesAuto = a[i];
	}

	return teuerstesAuto;
}

/*
3c)	Die Funktion kapital soll aus den n Elementen des Feldes a die Summe aller Gebrautpreise berechnen und zur�ck liefern.
*/
double kapital(Auto a[], int n)
{
	double summe = 0.0;

	for (size_t i = 0; i < n; i++)
	{
		summe += a[i].gebrauchtPreis();
	}

	return summe;
}




